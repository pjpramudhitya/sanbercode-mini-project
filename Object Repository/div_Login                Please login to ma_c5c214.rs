<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login                Please login to ma_c5c214</name>
   <tag></tag>
   <elementGuidId>79a1999e-577f-461e-b02c-c666bb76e5a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='login']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-12.text-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c098d504-aa0c-4360-b896-bd7222af853c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-12 text-center</value>
      <webElementGuid>2f24ddae-dc57-4d4a-8cc5-b9417897e1fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Login
                Please login to make appointment.
                            </value>
      <webElementGuid>60e03338-ee71-4c5d-b8de-fc37d5ce86e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 text-center&quot;]</value>
      <webElementGuid>75ff1aa0-97a9-4fc5-aeb1-af0200649bf8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div</value>
      <webElementGuid>07cdfd4a-2ffc-4af9-a985-703dd3a4996b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[3]</value>
      <webElementGuid>c535afa9-1a69-4d9f-a666-d8a2d5c7ad13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[3]</value>
      <webElementGuid>f8c82a72-f808-477e-be28-ff817e077c87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Demo account'])[1]/preceding::div[1]</value>
      <webElementGuid>d6766377-7bc5-44dd-aca4-f30d7596c379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
      <webElementGuid>aed127a1-66bb-4dd9-ac9f-782db6ab4e57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Login
                Please login to make appointment.
                            ' or . = '
                Login
                Please login to make appointment.
                            ')]</value>
      <webElementGuid>dc374e0c-a54a-4e09-8b20-d59f540cefca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
