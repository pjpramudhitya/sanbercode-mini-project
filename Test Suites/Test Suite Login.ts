<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>61d51cee-b7aa-4631-99eb-bb9094fbb0f1</testSuiteGuid>
   <testCaseLink>
      <guid>f4d35884-9460-4d2f-8158-1db19774a336</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/TC_LOGIN_001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>00c86141-422a-48c0-9f4c-8aad039baae5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/TC_LOGIN_002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b51b02bb-420e-417b-9e43-4b660c4e2ea0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/TC_LOGIN_003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>99a244c0-4e40-4415-8af6-622c83823c4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/TC_LOGIN_004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b35940bb-f6ef-4886-9094-d8e41fb7a1b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/TC_LOGIN_005</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bb985401-733d-44b0-9781-5f480fa726c1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>bb985401-733d-44b0-9781-5f480fa726c1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>f9de0c94-afe3-4cb5-b56c-cc0aa11e1848</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bb985401-733d-44b0-9781-5f480fa726c1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>9b9524b0-cb8b-427d-83f6-a54f58e0137b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
